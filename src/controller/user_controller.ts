import { Request, Response } from "express";
import { UserSchema } from "types/user_schema";
import { ResponseMessage, StatusCode } from "@infrastructure/enums";
import { HydratedDocument } from "mongoose";
import { User } from "@models/user_schema";
import {compare, hash} from "bcrypt";
import {PayloadSchema} from "types/payload_schema";

export const createUser = async (req: Request, res: Response) => {
  const { names, lastnames, email, password, public_id }: UserSchema = req.body;

  const user: HydratedDocument<UserSchema> = new User({
    email,
    lastnames,
    names,
    password: await hash(password, 9),
  });

  try {
    await user.save();

    return res
      .status(StatusCode.SUCCESS)
      .json({ message: ResponseMessage.CREATE_USER });
  } catch ({ errors }) {
    return res.status(StatusCode.BAD_REQUEST).json({ errors });
  }
};

export const signIn = async (req: Request, res: Response) => {
  const { email, password }: UserSchema = req.body;

  try {
    const user = await User.findOne<UserSchema>({ email });

    return res.status(StatusCode.SUCCESS).json({ user });
  } catch (e) {
    return res.status(StatusCode.BAD_REQUEST).json({ e });
  }
};

export const getToken =  (password: string, hash: string) => {
  const is_logged = compare(password, hash);

  const payload: PayloadSchema = {

  }


}