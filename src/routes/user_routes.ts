import { Router } from "express";
import { createUser, signIn } from "@controller/user_controller";

const router = Router();

router.post("/user", createUser);
router.post("/user/sign-in", signIn);

export { router };
