import { Schema, model } from "mongoose";
import { UserSchema } from "types/user_schema";
import { NameSchemas, ValidationSchemas } from "@infrastructure/enums";
import moment from "moment";

const UserSchema = new Schema<UserSchema>({
  created_at: {
    type: Number,
    default: moment().unix() * 1000,
  },

  lastnames: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    unique: true,
    trim: true,
    validate: {
      validator: (v) => /^\w+([\-]?\w+)*@\w+([\-]?w+)*(\.\w{2,3})+$/.test(v),
      message: ValidationSchemas.EMAIL,
    },
  },
  names: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  private_id: {
    type: String,
  },
  updated_at: {
    type: Number,
    default: null,
  },
});

export const User = model(NameSchemas.USER, UserSchema);
