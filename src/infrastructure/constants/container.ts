import * as dotenv from "dotenv";
import mongoose from "mongoose";
dotenv.config();

export const server = (port: number): string =>
  `Server is listening on port ${port}`;

export const mongodb_success = (): string => `MongoDB connected`;

export const mongo = mongoose.connect(`${process.env.PD_MONGO_URI}/${process.env.PD_STAGE}-pd-api-cms`);
