import * as dotenv from "dotenv";

dotenv.config();
import express from "express";
import cors from "cors";
import { PortEnum } from "@infrastructure/enums";
import { mongo, mongodb_success, server } from "@infrastructure/constants";

export const app = express();
const port = process.env.PORT || PortEnum.PORT;

/* Middlewares */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    origin: true,
    credentials: true,
  }),
);

/* Routes */
import * as UserRoutes from "@routes/user_routes";
export const router: express.Router[] = [UserRoutes.router];

app.use("/api/v1", [...router]);

app.listen(port, () => {
  console.log(server(PortEnum.PORT));
  mongo
    .then(() => console.log(mongodb_success()))
    .catch((err) => {
      throw err;
    });
});
